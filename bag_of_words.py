import numpy as np
from sklearn.cluster import KMeans
from itertools import compress
from tqdm import tqdm
from sklearn import neighbors as knn
from scipy.ndimage import correlate
from pre_processing import partition
import math
import cv2


class BagOfWords:

    def __init__(self, n_clusters, patch_size):
        self.fingerprints = None
        self.visual_words = None
        self.n_clusters = n_clusters
        self.labels = None
        self.patch_size = patch_size
        self.filter_bank = []
        self.filter_size = [25, 25]
        self.generate_filter_bank()
        self.threshold = None

    @staticmethod
    def get_name():
        return "Bag of words"

    def generate_filter_bank(self):
        """
        Initialises the filter bank.
        Creates multiple different filters to detect different shapes.
        """
        # Gaussian Blob Detectors
        for scale in [1, 2, 4]:
            self.complete_filter(self.fspecial_gauss(2 * math.ceil(scale * 2.5) + 1, scale))
        # Averaging Large Blob Detectors
        for scale in [25]:
            self.complete_filter(np.full((scale, scale), 1 / 3 ** 2))
        # Difference of Gaussian (DoG) General Edge Detectors
        for scale in [1, 2, 4]:
            self.complete_filter(self.fspecial_log(2 * math.ceil(scale * 2.5) + 1, scale))
        # Derivative 0 degree Edge Detectors
        for scale in [1, 2, 4]:
            f = self.fspecial_gauss(2 * math.ceil(scale * 2.5) + 1, scale)
            f = correlate(f, np.array([[-1, 0, 1]]), mode='constant', cval=0)
            self.complete_filter(f)
        # Derivative 90 degree Edge Detectors
        for scale in [1, 2, 4]:
            f = self.fspecial_gauss(2 * math.ceil(scale * 2.5) + 1, scale)
            f = correlate(f, np.array([[1], [0], [-1]]), mode='constant', cval=0)
            self.complete_filter(f)
        # Streak Detector 1
        f = np.zeros((15, 15))
        f[5] = [1] * 15
        f[7] = [-1] * 15
        f[9] = [-1] * 15
        f[11] = [1] * 15
        self.complete_filter(f)
        # Streak Detector 2
        f = np.zeros((15, 15))
        f[4] = [1] * 15
        f[6] = [-1] * 15
        f[10] = [-1] * 15
        f[12] = [1] * 15
        self.complete_filter(f)
        # Streak Detector 3
        f = np.zeros((15, 15))
        f[3] = [1] * 15
        f[5] = [-1] * 15
        f[11] = [-1] * 15
        f[13] = [1] * 15
        self.complete_filter(f)
        # Hop Detector
        f_frame = [-1, 0, 1]
        f = np.zeros((25, 25))
        for i in range(25):
            f[i][11:14] = f_frame
        self.complete_filter(f)

    @staticmethod
    def fspecial_gauss(size, sigma):
        """
        Function to mimic the 'fspecial' gaussian MATLAB function
        """
        x, y = np.mgrid[-size // 2 + 1:size // 2 + 1, -size // 2 + 1:size // 2 + 1]
        g = np.exp(-((x ** 2 + y ** 2) / (2.0 * sigma ** 2)))
        return g / g.sum()

    @staticmethod
    def fspecial_log(size, sigma):
        """
        Function to mimic the 'fspecial' log MATLAB function
        """
        x, y = np.mgrid[-size // 2 + 1:size // 2 + 1, -size // 2 + 1:size // 2 + 1]
        radsqrd = x ** 2 + y ** 2
        f = -1 / (math.pi * sigma ** 4) * (1 - radsqrd / (2 * sigma ** 2)) * np.exp(-radsqrd / (2 * sigma ** 2))
        return f - f.mean()

    def complete_filter(self, f):
        f = f / sum(sum(abs(f)))
        m, _ = f.shape
        pad = int((self.filter_size[0] - m) / 2)
        f = np.pad(f, ((pad, pad), (0, 0)))
        self.filter_bank.append(f)

    def fit(self, images, train_labels):
        """
        Trains the model and creates a visual word, and fingerprint dictionary.
        First it applies the filters on the images.
        Creates clusters for the response vectors, calculates mean response vectors and stores it as visual words.
        Finally, creates fingerprints of every input image.
        :param images: The training images
        :param train_labels: The labels of the training images.
        """
        self.labels = train_labels
        features = [self.apply_filter_bank(x) for x in tqdm(images)]
        list_features = [x for xs in [x for xs in features for x in xs] for x in xs]
        kmeans = KMeans(n_clusters=self.n_clusters, n_init=5)
        labels = kmeans.fit_predict(list_features)
        print("Clusters created")
        clusters = [list(compress(list_features, labels[labels == label])) for label in np.unique(labels)[1:]]
        self.visual_words = knn.NearestNeighbors(n_neighbors=1)
        self.visual_words.fit([np.array(cluster).mean(axis=0) for cluster in clusters])
        self.create_fingerprint(features, training=True)
        print("Training complete")
        return True

    def apply_filter_bank(self, image):
        """
        Executes the filters from the filter bank on an image.
        :param image: The image.
        :return: Vector with all the results from the different filters.
        """
        responses = []
        for f in self.filter_bank:
            responses.append(cv2.filter2D(image, -1, f))
        return np.stack(responses, axis=-1)

    def create_fingerprint(self, images, training=False):
        """
        Constructs the fingerprints of the list of images.
        Checks which visual word is closest for every pixel. Counts how much every visual word is encountered.
        :param images: The images to create fingerprints of.
        :param training: Boolean indicating whether the fingerprints need to be saved or not.
        :return: A list of all fingerprints.
        """
        fingerprints = []
        for image in images:
            fingerprint = [0] * self.n_clusters
            for row in image:
                clusters = self.visual_words.kneighbors(row, return_distance=False)
                for pixel in clusters:
                    fingerprint[pixel[0]] += 1
            res = sum(fingerprint)
            fingerprints.append([x / res for x in fingerprint])
        if training:
            self.fingerprints = knn.NearestNeighbors(n_neighbors=3)
            self.fingerprints.fit(fingerprints, self.labels)
        return fingerprints

    def prediction_score(self, image):
        """
        Calculates the percentage of patches with a detected anomaly within an image.
        :param image: The image
        :return: The percentage of anomaly patches
        """
        preds = self.predict_patches(partition(image, self.patch_size, self.patch_size))
        return sum(preds) / len(preds)

    def predict_patches(self, patches):
        """
        Looks for re-coater streaking in a list of patches.
        Extracts the features, creates the fingerprints and checks which 3 fingerprints are most similar.
        :param patches: The patches to detect re-coater streaking on.
        :return: An array of classifications.
        """
        features = [self.apply_filter_bank(x) for x in patches]
        fingerprints = self.create_fingerprint(features)
        neighbours = self.fingerprints.kneighbors(fingerprints, return_distance=False)
        return [min(self.labels[y] for y in x) for x in neighbours]

    def indicate_streaking(self, img):
        """
        Finds instances of recoater streaking in an image and returns the indices where it occurs.
        :param img: The image
        :return: The indices of recoater streaking
        """
        corners = []
        _, w = img.shape
        preds = self.predict_patches(partition(img, self.patch_size, self.patch_size))
        for i in range(len(preds)):
            if preds[i] == 1:
                hor = (i % (w // self.patch_size)) * self.patch_size
                ver = (i // (w // self.patch_size)) * self.patch_size
                corners.append([hor, hor+self.patch_size, ver, ver+self.patch_size])
        return corners

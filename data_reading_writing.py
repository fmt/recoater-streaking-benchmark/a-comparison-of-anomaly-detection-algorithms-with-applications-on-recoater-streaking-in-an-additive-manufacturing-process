import cv2
import numpy as np
import os
import pre_processing
from tqdm import tqdm
from line_profiles import LineProfiles
from sklearn.model_selection import train_test_split


def read_data(zoom=None, pre="None", validate=True):
    """
    Reads the images and corresponding labels of the training and either validation or test sets.
    :param zoom: Indicates which zoom level is loaded. Has to be exact.
    :param pre: Indicates which pre-processing is loaded in. Has to be exact.
    :param validate: Indicates whether the validation or test set is loaded.
    :return: lists of training and validation (or test) data and labels
    """
    x_train, x_test, y_train, y_test = [], [], [], []
    path = os.path.join("Data/training", pre) if zoom is None else os.path.join("Data/training", zoom, pre)
    for picture in tqdm(os.listdir(os.path.join(path, "RCS"))):
        image = os.path.join(path, "RCS", picture)
        x_train.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
        y_train.append(1)
    for picture in tqdm(os.listdir(os.path.join(path, "no_RCS"))):
        image = os.path.join(path, "no_RCS", picture)
        x_train.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
        y_train.append(0)
    path = os.path.join("Data/validation", pre) if validate else os.path.join("Data/test", pre)
    for picture in tqdm(os.listdir(os.path.join(path, "RCS"))):
        image = os.path.join(path, "RCS", picture)
        x_test.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
        y_test.append(1)
    for picture in tqdm(os.listdir(os.path.join(path, "no_RCS"))):
        image = os.path.join(path, "no_RCS", picture)
        x_test.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
        y_test.append(0)

    print("Data has been loaded in.")
    return x_train, x_test, y_train, y_test


def read_datasets(training_prints, testing_sets):
    train_data, train_labels = [], []
    for print_job in training_prints:
        print(f"Reading job {print_job}")
        data_print, labels = read_whole_print(print_job)
        train_data.extend(data_print)
        train_labels.extend(labels)
    x_train, x_val, y_train, y_val = train_test_split(train_data, train_labels, train_size=0.8)
    x_test, y_test = [], []
    for print_job in testing_sets:
        print(f"Reading job {print_job}")
        data_print, labels = read_whole_print(print_job)
        x_test.extend(data_print)
        y_test.extend(labels)
    return x_train, y_train, x_val, y_val, x_test, y_test


def read_whole_print(print_name):
    """
    Reads all images of a single print and sorts them chronologically.
    :param print_name: The name of the print.
    :return: The data of said print with corresponding labels.
    """
    data_print, labels, order = [], [], []
    path = os.path.join("Data/Whole_prints", print_name)
    if os.path.isdir(os.path.join(path, "RCS")):
        for picture in tqdm(os.listdir(os.path.join(path, "RCS"))):
            image = os.path.join(path, "RCS", picture)
            data_print.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
            labels.append(1)
            order.append(int(picture[:-4]))
    for picture in tqdm(os.listdir(os.path.join(path, "no_RCS"))):
        image = os.path.join(path, "no_RCS", picture)
        data_print.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
        labels.append(0)
        order.append(int(picture[:-4]))
    data_print = [x for _, x in sorted(zip(order, data_print))]
    labels = [x for _, x in sorted(zip(order, labels))]
    return data_print, labels


def generate_multi_scales(data, model, path):
    """
    Creates the training dataset for the Multi Scale network.
    Saves different scale sizes of patches in the RGB channels.
    :param data: The dataset to generate the scales of.
    :param model: To indicate RCS in the images.
    :param path: The path where to save the dataset
    :return: The multi scale dataset.
    """
    scaled_data = []
    h, w = data[0].shape

    os.makedirs(path)
    os.makedirs(os.path.join(path, "RCS"))
    os.makedirs(os.path.join(path, "no_RCS"))
    rcs, norcs = 1, 1

    for image in tqdm(data):
        indices = model.indicate_streaking(image)[0]
        indices = np.unique([x // 25 for x in indices])
        scaled_image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_LINEAR)
        padded = cv2.copyMakeBorder(image, 40, 40, 40, 40, cv2.BORDER_REFLECT)
        for i in range(h // 25):
            for j in range(w // 25):
                scale_1 = image[i * 25:(i + 1) * 25, j * 25:(j + 1) * 25]
                scale_2 = padded[i * 25:i * 25 + 100, j * 25:j * 25 + 100]

                scale_1_resize = cv2.resize(scale_1, (256, 256), interpolation=cv2.INTER_LINEAR)
                scale_2_resize = cv2.resize(scale_2, (256, 256), interpolation=cv2.INTER_LINEAR)

                final_patch = np.uint8(np.dstack((scaled_image, scale_1_resize, scale_2_resize)))
                scaled_data.append(final_patch)

                if i in indices:
                    cv2.imwrite(os.path.join(path, "RCS", str(rcs) + ".png"), final_patch)
                    rcs += 1
                else:
                    cv2.imwrite(os.path.join(path, "no_RCS", str(norcs) + ".png"), final_patch)
                    norcs += 1
    return scaled_data


def generate_scales_scime(image):
    """
    Generates the image patches to be used by the method from Scime et al.
    Partitions the image in 25x25 patches, and extracts 100x100 patches with the same centres.
    Finally all patches are resized and 3D images are constructed with the two patches and the whole image as dimensions
    :param image: The image to extract the patches from.
    :return: The extracted patches.
    """
    result = []
    h, w = image.shape
    scaled_image = cv2.resize(image, (227, 227), interpolation=cv2.INTER_LINEAR)
    padded = cv2.copyMakeBorder(image, 40, 40, 40, 40, cv2.BORDER_REFLECT)
    for i in range(h // 25):
        for j in range(w // 25):
            scale_1 = image[i * 25:(i + 1) * 25, j * 25:(j + 1) * 25]
            scale_2 = padded[i * 25:i * 25 + 100, j * 25:j * 25 + 100]

            scale_1_resize = cv2.resize(scale_1, (227, 227), interpolation=cv2.INTER_LINEAR)
            scale_2_resize = cv2.resize(scale_2, (227, 227), interpolation=cv2.INTER_LINEAR)

            result.append(np.dstack((scaled_image, scale_1_resize, scale_2_resize)))
    return result


def generate_scales_yadav(image):
    """
    Generates the image patches to be used by the method from Yadav et al.
    Partitions the image in 75x75 patches, and extracts 150x150 patches with the same centres.
    :param image: The image to extract the patches from.
    :return: The extracted patches.
    """
    result = []
    h, w = image.shape
    padded = cv2.copyMakeBorder(image, 40, 40, 40, 40, cv2.BORDER_REFLECT)
    for i in range(h // 75):
        for j in range(w // 75):
            scale_1 = image[i * 75:(i + 1) * 75, j * 75:(j + 1) * 75]
            scale_2 = padded[i * 75:i * 75 + 150, j * 75:j * 75 + 150]

            scale_1_resize = cv2.resize(scale_1, (150, 150), interpolation=cv2.INTER_LINEAR)

            result.append(scale_1_resize)
            result.append(scale_2)
    return result


def create_partitions(data, labels, partition_height, partition_width):
    """
    Creates a partition of the training dataset.
    Uses the LineProfiles model to label the created partitions.
    :param data: The data to partition
    :param labels: The labels corresponding to the data
    :param partition_height: Height of the scales.
    :param partition_width: Width of the scales.
    :return: The new dataset with corresponding labels
    """
    h, w = data[0].shape

    craeghs = LineProfiles()
    craeghs.fit(data, labels)
    # Deletes entries that it classifies incorrectly to minimize wrong labels further on.
    x_train, _ = delete_uncertain_labels(data, labels, craeghs)

    partitioned, new_labels = [], []
    for img in tqdm(x_train):
        indices = craeghs.indicate_streaking(img)[0]
        indices = np.unique([x // partition_height for x in indices])
        ver = (w // partition_width)
        for i in range(h // partition_height):
            new_labels += [1] * ver if i in indices else [0] * ver
        partitioned.append(pre_processing.partition(img, partition_height, partition_width))
    partitioned = [x for xs in partitioned for x in xs]
    craeghs.fit(partitioned, new_labels)
    return delete_uncertain_labels(partitioned, new_labels, craeghs)


def delete_uncertain_labels(data, labels, model):
    """
    Given a dataset, deletes the data points that the model classifies incorrectly.
    This is done to minimize wronly labeled data.
    :param data: The data.
    :param labels: Corresponding labels.
    :param model: The model for classification.
    :return: The dataset and labels without the incorrectly classified ones.
    """
    preds = model.predict(data)
    idx = [i for i in range(len(preds)) if preds[i] == labels[i]]
    return [data[i] for i in idx], [labels[i] for i in idx]


def save_dataset(data, labels, path):
    """
    Saves a newly created dataset for later use

    :param data: The dataset to save
    :param labels: The corresponding labels
    :param path: The path where to save the dataset
    """
    os.makedirs(path)
    os.makedirs(os.path.join(path, "RCS"))
    os.makedirs(os.path.join(path, "no_RCS"))
    rcs, norcs = 1, 1
    for img, label in tqdm(zip(data, labels)):
        if label == 1:
            cv2.imwrite(os.path.join(path, "RCS", str(rcs) + ".png"), img)
            rcs += 1
        else:
            cv2.imwrite(os.path.join(path, "no_RCS", str(norcs) + ".png"), img)
            norcs += 1

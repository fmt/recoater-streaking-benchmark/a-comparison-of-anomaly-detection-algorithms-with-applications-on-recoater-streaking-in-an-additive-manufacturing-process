import benchmarks
from data_reading_writing import generate_multi_scales
from data_reading_writing import create_partitions
from data_reading_writing import read_datasets
from data_reading_writing import save_dataset
import pre_processing as prep
import cv2
from tqdm import tqdm


def create_datasets():
    """
    Creates datasets and saves them for later use.
    The first datasets are used by Line Profiles, AlexNet, EfficientNet, and Local Binary Patterns methods.

    The 150x150 and 75x75 partitioned datasets are used by the Triple-Scale network.
    The 'Scime' partition are used by the Multi-Scale network.
    The 20x20 paritions are used by the Bag-of-Words method.
    """
    x_train, y_train, x_val, y_val, x_test, y_test = read_datasets(training_prints=['F04', 'F12', 'F13', 'F16', 'F19', 'F20', 'F22'],
                                                                   testing_sets=['F14', 'F15', 'F17'])

    print("Starting Basic Mask preprocessing")
    basic_mask = cv2.imread("Data/mask.png", cv2.IMREAD_GRAYSCALE)
    x_train_mask = [prep.yin(x, basic_mask) for x in tqdm(x_train)]
    x_val_mask = [prep.yin(x, basic_mask) for x in tqdm(x_val)]
    x_test_mask = [prep.yin(x, basic_mask) for x in tqdm(x_test)]

    print("Starting Image Morphology preprocessing")
    x_train_morphology = [prep.smoothen(x) for x in tqdm(x_train)]
    x_val_morphology = [prep.smoothen(x) for x in tqdm(x_val)]
    x_test_morphology = [prep.smoothen(x) for x in tqdm(x_test)]

    print("Starting saving images")
    save_dataset(x_train, y_train, 'Data/training/None')
    save_dataset(x_train_mask, y_train, 'Data/training/Masking')
    save_dataset(x_train_morphology, y_train, 'Data/training/Morphology')

    save_dataset(x_test, y_test, 'Data/test/None')
    save_dataset(x_test_mask, y_test, 'Data/test/Masking')
    save_dataset(x_test_morphology, y_test, 'Data/test/Morphology')

    save_dataset(x_val, y_val, 'Data/validation/None')
    save_dataset(x_val_mask, y_val, 'Data/validation/Masking')
    save_dataset(x_val_morphology, y_val, 'Data/validation/Morphology')

    print("Starting partitioning 75x75")
    x_train_75_75, y_train_75_75 = create_partitions(x_train, y_train, 75, 75)
    x_val_75_75, y_val_75_75 = create_partitions(x_val, y_val, 75, 75)
    x_test_75_75, y_test_75_75 = create_partitions(x_test, y_test, 75, 75)
    save_dataset(x_train_75_75, y_train_75_75, 'Data/training/75x75/None')
    save_dataset(x_val_75_75, y_val_75_75, 'Data/validation/75x75/None')
    save_dataset(x_test_75_75, y_test_75_75, 'Data/test/75x75/None')

    x_train_mask_75_75, y_train_mask_75_75 = create_partitions(x_train_mask, y_train, 75, 75)
    x_val_mask_75_75, y_val_mask_75_75 = create_partitions(x_val_mask, y_val, 75, 75)
    x_test_mask_75_75, y_test_mask_75_75 = create_partitions(x_test_mask, y_test, 75, 75)
    save_dataset(x_train_mask_75_75, y_train_mask_75_75, 'Data/training/75x75/Masking')
    save_dataset(x_val_mask_75_75, y_val_mask_75_75, 'Data/validation/75x75/Masking')
    save_dataset(x_test_mask_75_75, y_test_mask_75_75, 'Data/test/75x75/Masking')

    x_train_morphology_75_75, y_train_morphology_75_75 = create_partitions(x_train_morphology, y_train, 75, 75)
    x_val_morphology_75_75, y_val_morphology_75_75 = create_partitions(x_val_morphology, y_val, 75, 75)
    x_test_morphology_75_75, y_test_morphology_75_75 = create_partitions(x_test_morphology, y_test, 75, 75)
    save_dataset(x_train_morphology_75_75, y_train_morphology_75_75, 'Data/training/75x75/Morphology')
    save_dataset(x_val_morphology_75_75, y_val_morphology_75_75, 'Data/validation/75x75/Morphology')
    save_dataset(x_test_morphology_75_75, y_test_morphology_75_75, 'Data/test/75x75/Morphology')

    print("Starting partitioning 150x150")
    x_train_150_150, y_train_150_150 = create_partitions(x_train, y_train, 150, 150)
    x_val_150_150, y_val_150_150 = create_partitions(x_val, y_val, 150, 150)
    x_test_150_150, y_test_150_150 = create_partitions(x_test, y_test, 150, 150)
    save_dataset(x_train_150_150, y_train_150_150, 'Data/training/150x150/None')
    save_dataset(x_val_150_150, y_val_150_150, 'Data/validation/150x150/None')
    save_dataset(x_test_150_150, y_test_150_150, 'Data/test/150x150/None')

    x_train_mask_150_150, y_train_mask_150_150 = create_partitions(x_train_mask, y_train, 150, 150)
    x_val_mask_150_150, y_val_mask_150_150 = create_partitions(x_val_mask, y_val, 150, 150)
    x_test_mask_150_150, y_test_mask_150_150 = create_partitions(x_test_mask, y_test, 150, 150)
    save_dataset(x_train_mask_150_150, y_train_mask_150_150, 'Data/training/150x150/Masking')
    save_dataset(x_val_mask_150_150, y_val_mask_150_150, 'Data/validation/150x150/Masking')
    save_dataset(x_test_mask_150_150, y_test_mask_150_150, 'Data/test/150x150/Masking')

    x_train_morphology_150_150, y_train_morphology_150_150 = create_partitions(x_train_morphology, y_train, 150, 150)
    x_val_morphology_150_150, y_val_morphology_150_150 = create_partitions(x_val_morphology, y_val, 150, 150)
    x_test_morphology_150_150, y_test_morphology_150_150 = create_partitions(x_test_morphology, y_test, 150, 150)
    save_dataset(x_train_morphology_150_150, y_train_morphology_150_150, 'Data/training/150x150/Morphology')
    save_dataset(x_val_morphology_150_150, y_val_morphology_150_150, 'Data/validation/150x150/Morphology')
    save_dataset(x_test_morphology_150_150, y_test_morphology_150_150, 'Data/test/150x150/Morphology')

    print("Starting partitioning 20x20")
    x_train_20_20, y_train_20_20 = create_partitions(x_train, y_train, 20, 20)
    x_val_20_20, y_val_20_20 = create_partitions(x_val, y_val, 20, 20)
    x_test_20_20, y_test_20_20 = create_partitions(x_test, y_test, 20, 20)
    save_dataset(x_train_20_20, y_train_20_20, 'Data/training/20x20/None')
    save_dataset(x_val_20_20, y_val_20_20, 'Data/validation/20x20/None')
    save_dataset(x_test_20_20, y_test_20_20, 'Data/test/20x20/None')

    x_train_mask_20_20, y_train_mask_20_20 = create_partitions(x_train_mask, y_train, 20, 20)
    x_val_mask_20_20, y_val_mask_20_20 = create_partitions(x_val_mask, y_val, 20, 20)
    x_test_mask_20_20, y_test_mask_20_20 = create_partitions(x_test_mask, y_test, 20, 20)
    save_dataset(x_train_mask_20_20, y_train_mask_20_20, 'Data/training/20x20/Masking')
    save_dataset(x_val_mask_20_20, y_val_mask_20_20, 'Data/validation/20x20/Masking')
    save_dataset(x_test_mask_20_20, y_test_mask_20_20, 'Data/test/20x20/Masking')

    x_train_morphology_20_20, y_train_morphology_20_20 = create_partitions(x_train_morphology, y_train, 20, 20)
    x_val_morphology_20_20, y_val_morphology_20_20 = create_partitions(x_val_morphology, y_val, 20, 20)
    x_test_morphology_20_20, y_test_morphology_20_20 = create_partitions(x_test_morphology, y_test, 20, 20)
    save_dataset(x_train_morphology_20_20, y_train_morphology_20_20, 'Data/training/20x20/Morphology')
    save_dataset(x_val_morphology_20_20, y_val_morphology_20_20, 'Data/validation/20x20/Morphology')
    save_dataset(x_test_morphology_20_20, y_test_morphology_20_20, 'Data/test/20x20/Morphology')

    print("Starting Scime partitioning")
    generate_multi_scales(x_train, y_train, 'Data/training/scime/None')
    generate_multi_scales(x_train_morphology, y_train, 'Data/training/scime/Morphology')
    generate_multi_scales(x_train_mask, y_train, 'Data/training/scime/Masking')

    generate_multi_scales(x_test, y_test, 'Data/test/scime/None')
    generate_multi_scales(x_test_morphology, y_test, 'Data/test/scime/Morphology')
    generate_multi_scales(x_test_mask, y_test, 'Data/test/scime/Masking')

    generate_multi_scales(x_val, y_val, 'Data/validation/scime/None')
    generate_multi_scales(x_val_morphology, y_val, 'Data/validation/scime/Morphology')
    generate_multi_scales(x_val_mask, y_val, 'Data/validation/scime/Masking')


def benchmarking(methods, pre="None"):
    if 'Line Profiles Moving Average' in methods:
        benchmarks.benchmark_craeghs(method="moving", pre=pre)
    if 'Line Profiles Fitted Function' in methods:
        benchmarks.benchmark_craeghs(method="fitted", pre=pre)
    if 'Local Binary Patterns' in methods:
        benchmarks.benchmark_yin(block_sizes=[6], part_sizes=[15], pre=pre)
    if 'Bag-of-Words' in methods:
        benchmarks.benchmark_bow(pre=pre)
    if 'AlexNet' in methods:
        benchmarks.benchmark_shi(epochs=20, pre=pre)
    if 'Triple-Scale' in methods:
        benchmarks.benchmark_yadav(epochs=15, pre=pre)
    if 'Multi-Scale' in methods:
        benchmarks.benchmark_scime(epochs=15, pre=pre)
    if 'EfficientNet' in methods:
        benchmarks.benchmark_efficientnet(epochs=20, pre=pre)


if __name__ == "__main__":
    create_datasets()

    # Which pre-processing method to use.
    # "None" for no pre-processing
    # "Masking" for basic mask pre-processing
    # "Morphology" for image morphology
    preprocess = "None"
    benchmarking(methods=['Line Profiles Moving Average',
                          'Line Profiles Fitted Function',
                          'Local Binary Patterns',
                          'Bag-of-Words',
                          'AlexNet',
                          'Triple-Scale',
                          'Multi-Scale',
                          'EfficientNet'],
                 pre=preprocess)

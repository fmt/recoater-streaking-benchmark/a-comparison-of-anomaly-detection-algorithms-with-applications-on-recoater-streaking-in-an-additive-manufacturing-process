import tensorflow as tf
from tensorflow import keras
from keras.utils import image_dataset_from_directory
from data_reading_writing import generate_scales_scime
from visualization import neural_network_training_plot
from tqdm import tqdm
import data_augementation as dg
import numpy as np


class MulitScale:

    def __init__(self, epochs):
        """
        Initialization of the AlexNet.
        """
        self.epochs = epochs
        self.model = keras.models.Sequential([
            keras.layers.Conv2D(filters=96, kernel_size=(11, 11), strides=(4, 4), activation='relu',
                                input_shape=(227, 227, 3)),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2)),
            keras.layers.Conv2D(filters=256, kernel_size=(5, 5), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2)),
            keras.layers.Conv2D(filters=384, kernel_size=(3, 3), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.Conv2D(filters=384, kernel_size=(3, 3), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2)),
            keras.layers.Flatten(),
            keras.layers.Dense(4096, activation='relu'),
            keras.layers.Dropout(0.5),
            keras.layers.Dense(4096, activation='relu'),
            keras.layers.Dropout(0.5),
            keras.layers.Dense(2, activation='softmax')
        ])
        self.model.compile(loss='categorical_crossentropy',
                           optimizer=tf.optimizers.SGD(learning_rate=0.001, momentum=0.9),
                           metrics=['accuracy'])

    @staticmethod
    def get_name():
        return "Multi-scale"

    def fit_from_path(self, training_path, validation_path, plot_losses=False, train=True):
        """
        Trains the network, and validates it, with datasets from their directories.
        Increases the training set by adding mirrored and flipped images and using random crops.
        :param training_path: The path to the training dataset.
        :param validation_path: The path to the validation dataset.
        :param plot_losses: Boolean indicating whether or not to plot the training history.
        :param train: Boolean indicating whether or not to retrain the model.
        """
        if not train:
            self.model = tf.keras.models.load_model('best_model_scime.h5')
            return True
        training_ds = image_dataset_from_directory(training_path,
                                                   image_size=(227, 227),
                                                   batch_size=32,
                                                   class_names=["no_RCS", "RCS"],
                                                   shuffle=True,
                                                   label_mode='categorical')

        # Increasing the training set by adding mirrored and flipped images
        training_ds = training_ds.concatenate(training_ds.map(dg.mirror_map))
        training_ds = training_ds.concatenate(training_ds.map(dg.flip_map))

        # Increasing the training set by adding images with adjusted brightness
        train_ds = training_ds.concatenate(training_ds.map(dg.bright_map_1))
        train_ds = train_ds.concatenate(training_ds.map(dg.bright_map_2))
        train_ds = train_ds.map(dg.standardize_map)

        val_ds = image_dataset_from_directory(validation_path,
                                              image_size=(227, 227),
                                              batch_size=32,
                                              class_names=["no_RCS", "RCS"],
                                              shuffle=True,
                                              label_mode='categorical')
        val_ds = val_ds.map(dg.standardize_map)
        callback = tf.keras.callbacks.ModelCheckpoint('best_model_scime.h5', monitor='val_loss', save_best_only=True)
        history = self.model.fit(train_ds,
                                 epochs=self.epochs,
                                 validation_data=val_ds,
                                 validation_steps=10,
                                 steps_per_epoch=100,
                                 callbacks=[callback])
        self.model = tf.keras.models.load_model('best_model_scime.h5')

        if plot_losses:
            neural_network_training_plot(history)

    def prediction_score(self, test_data):
        """
        Returns the prediction score for a list of images.
        :param test_data: An array of images to calculate the prediction scores off
        :return: The prediction scores
        """
        scores = []
        for x in tqdm(test_data):
            scales = generate_scales_scime(x)
            scales = [dg.standardize_map(x, 0)[0] for x in scales]
            results = self.model.predict(np.array(scales), verbose=0)
            preds = [np.argmax(x, axis=-1) for x in results]
            scores.append(sum(preds) / len(preds))
        return scores

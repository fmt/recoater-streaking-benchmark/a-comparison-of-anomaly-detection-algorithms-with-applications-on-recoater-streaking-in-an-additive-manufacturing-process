import numpy as np
from tqdm import tqdm
from sklearn import metrics
from data_reading_writing import read_data

from bag_of_words import BagOfWords
from alexnet import AlexNet
from multi_scale import MulitScale
from triple_scale import TripleScale
from EfficientNet import EfficientNet
from local_binary_pattern import LocalBinaryPattern
from line_profiles import LineProfiles


def benchmark_craeghs(method, pre=None):
    """
    Benchmarks the Line profiles method.
    :param method: Indicates whether the moving average of the fitted function should be used.
    :param pre: Which pre-processing method to use.
    """
    x_train, x_test, y_train, y_test = read_data(pre=pre, validate=False)
    lp = LineProfiles(method=method)
    lp.fit(x_train, y_train)
    scores = np.array([lp.prediction_score(x) for x in tqdm(x_test)])
    preds = lp.predict(x_test)

    fpr, tpr, thresholds = metrics.roc_curve(y_test, scores)

    tn, fp, fn, tp = metrics.confusion_matrix(y_test, preds).ravel()
    print(f"Line profiles with method {method} and {pre} pre-processing\n")
    confusion_matrix(tn, fp, fn, tp)
    print(f'Accuracy:  {metrics.accuracy_score(y_test, preds)} \n'
          f'Recall:    {metrics.recall_score(y_test, preds, zero_division=0)} \n'
          f'Precision: {metrics.precision_score(y_test, preds, zero_division=0)} \n'
          f'F1:        {metrics.f1_score(y_test, preds)}\n'
          f"Average precision: {metrics.average_precision_score(y_test, scores)}\n"
          f"AUC:       {metrics.auc(fpr, tpr)}")


def benchmark_shi(epochs=20, pre="None"):
    """
    Benchmarks the AlexNet method.
    :param epochs: The number of training epochs.
    :param pre: Which pre-processing to use
    """
    alexnet = AlexNet(epochs)
    alexnet.fit_from_path("Data/training/" + pre, "Data/validation/" + pre, train=True)
    scores, y_test = alexnet.prediction_score_from_path("Data/test/" + pre)
    evaluate(np.array(scores), y_test)


def benchmark_efficientnet(epochs=20, pre='None'):
    """
    Benchmarks the EfficientNet method.
    :param epochs: The number of training epochs.
    :param pre: Which pre-processing to use
    """
    ef = EfficientNet(epochs)
    ef.fit_from_path("Data/training/" + pre, "Data/validation/" + pre, train=True)
    scores, y_test = ef.prediction_score_from_path("Data/test/" + pre)
    evaluate(np.array(scores), y_test)


def benchmark_yadav(epochs=20, pre="None"):
    """
    Benchmarks the CNN method as proposed by Yadav.
    :param epochs: The number of training epochs.
    :param pre: Which pre-processing to use
    """
    _, x_test, _, y_test = read_data(pre=pre, validate=False)
    yadav = TripleScale(epochs)
    yadav.fit_from_path(pre=pre, train=True)
    scores = yadav.prediction_score(x_test)
    evaluate(np.array(scores), y_test)


def benchmark_scime(epochs=20, pre="None"):
    """
    Benchmarks the neural network of Scime.
    :param epochs: The number of training epochs.
    :param pre: Which pre-processing method to use.
    """
    _, x_test, _, y_test = read_data(pre=pre, validate=False)
    scime = MulitScale(epochs)
    scime.fit_from_path("Data/training/scime/" + pre, "Data/validation/scime/" + pre)
    scores = scime.prediction_score(x_test)
    evaluate(np.array(scores), y_test)


def benchmark_yin(block_sizes, part_sizes, pre=None):
    """
    Benchmarks the Local binary pattern method.
    :param block_sizes: The different block sizes for the Multi-Block LBP
    :param part_sizes: The different partition sizes for the Partition LBP
    :param pre: Which pre-processing method to use.
    """
    x_train, x_test, y_train, y_test = read_data(pre=pre, validate=False)
    for block in block_sizes:
        for part in part_sizes:
            lbp = LocalBinaryPattern(radius=1, block=block, partition_size=part)
            lbp.fit(x_train, y_train)
            scores = lbp.scores(x_test)
            print(f"Confusion matrix with {pre} pre-processing for radius 1, block size {block}x{block} "
                  f"and partition size {part}x{part}.")
            evaluate(scores, y_test)


def benchmark_bow(pre=None):
    """
    Benchmarks the Bag of Words method.
    :param pre: Which pre-processing method to use.
    """
    x_train, x_test, y_train, y_test = read_data(zoom="20x20", pre=pre, validate=False)
    bow = BagOfWords(n_clusters=100, patch_size=20)
    bow.fit(x_train, y_train)
    scores = np.array([bow.prediction_score(x) for x in tqdm(x_test)])
    evaluate(scores, y_test)


def evaluate(scores, labels):
    """
    Evaluates the prediction scores made by a model.
    Calculates the confusion matrix for the highest threshold and its metrics .
    Also calculates the area under the curve and average precision.
    :param scores: The prediction scores as list.
    :param labels: The ground truth.
    :return: False positive rate and true positive rate
    """
    fpr, tpr, thresholds = metrics.roc_curve(labels, scores)
    ap = metrics.average_precision_score(labels, scores)
    auc = metrics.auc(fpr, tpr)

    max_f1 = -1
    b_t = None
    for t in thresholds:
        pred = [0 if s < t else 1 for s in scores]
        f1 = metrics.f1_score(labels, pred)
        if f1 > max_f1:
            max_f1 = f1
            b_t = t

    preds = [0 if sc < b_t else 1 for sc in scores]

    tn, fp, fn, tp = metrics.confusion_matrix(labels, preds).ravel()
    confusion_matrix(tn, fp, fn, tp)
    print(f'Accuracy:  {metrics.accuracy_score(labels, preds)} \n'
          f'Recall:    {metrics.recall_score(labels, preds, zero_division=0)} \n'
          f'Precision: {metrics.precision_score(labels, preds, zero_division=0)} \n'
          f'F1:        {metrics.f1_score(labels, preds)}\n'
          f"Average precision: {ap}\n"
          f"AUC:       {auc}")
    return fpr, tpr


def confusion_matrix(tn, fp, fn, tp):
    """
    Constructs a confusion matrix and prints the accuracy, recall, precision, and F1.
    """
    print(f"\n___RCS___|   Pos  |   Neg\n"
          f"Positive | {tp:5}  | {fn:5} \n"
          f"Negative | {fp:5}  | {tn:5}")

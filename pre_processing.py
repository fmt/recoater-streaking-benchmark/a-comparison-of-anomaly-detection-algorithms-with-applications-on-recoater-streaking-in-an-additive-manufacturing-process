import matplotlib.pyplot as plt
import numpy as np
import cv2
from scipy import ndimage
from scipy.interpolate import interp1d


def histogram(image, show=False):
    """
    Plots a histogram of all pixel values.
    :param image: The image
    :param show: Boolean deciding whether the histogram is printed
    :return: Fitted function
    """
    values = np.reshape(image, -1)
    n, bins, _ = plt.hist(values, bins=150)
    function = interp1d(bins[:-1], n)
    plot = function(bins[:-1])
    if show:
        plt.plot(bins[:-1], plot)
        plt.show()
    return bins[:-1], plot


def smoothen(image):
    """
    Smooth the image by eroding the printed parts of the previous layer
    :param image: The image to smooth
    :return: The smoothed image
    """
    cols = image.shape[1]
    horizontal_size = cols // 30
    horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontal_size, 1))
    image = cv2.erode(image, horizontalStructure)
    image = cv2.dilate(image, horizontalStructure)
    return image


def partition(img, n, m):
    """
    Partitions an image into patches of NxM size.
    :param img: The image to split.
    :param n: Vertical length
    :param m: Horizontal length
    :return: patches of the image.
    """
    h, w = img.shape
    if not h % n == 0:
        """Rows not evenly divisible"""
        diff = h - np.mod(h, n)
        img = np.array(img[:diff])
    if not w % m == 0:
        """Columns not evenly divisible"""
        diff = w - np.mod(w, m)
        img = np.array([x[:diff] for x in img])
    return img.reshape((h // n, n, -1, m)).swapaxes(1, 2).reshape(-1, n, m)


def yin(image, mask):
    """
    Algorithm from Yin et al. Takes an image and compares it with a defect free basic mask.
    Ends with median filtering for noise reduction and gamma transformation for contrast enhancing.
    :param image: Image for pre processing
    :param mask: Basic mask to compare
    :return: Pre processed image
    """
    j = image - mask
    g1, g2 = 100, 255
    j[j > g2] = 0
    j[j < g1] = 0
    j = ndimage.median_filter(j, size=(3, 3))
    gamma = 2
    j = np.power(j, gamma)
    return j

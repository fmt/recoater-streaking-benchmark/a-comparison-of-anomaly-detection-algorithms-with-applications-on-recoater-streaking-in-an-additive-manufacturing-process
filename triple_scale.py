import tensorflow as tf
from tensorflow import keras
from keras.utils import image_dataset_from_directory
from tqdm import tqdm
from data_reading_writing import generate_scales_yadav
import data_augementation as dg
from visualization import neural_network_training_plot
import numpy as np


class TripleScale:
    model = None

    def __init__(self, epochs):
        """
        Initialization of the Neural Network.
        """
        self.epochs = epochs
        self.model = keras.models.Sequential([
            keras.layers.Conv2D(filters=8, kernel_size=(4, 4), strides=(1, 1), activation='relu',
                                input_shape=(150, 150, 1), padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(8, 8), strides=(8, 8)),
            keras.layers.Conv2D(filters=16, kernel_size=(8, 8), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(2, 2), strides=(2, 2)),
            keras.layers.Conv2D(filters=32, kernel_size=(8, 8), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.Flatten(),
            keras.layers.Dense(2592, activation='relu'),
            # keras.layers.Dropout(0.5),
            keras.layers.Dense(2, activation='softmax')
        ])
        self.model.compile(loss='categorical_crossentropy',
                           optimizer=tf.optimizers.SGD(learning_rate=0.001, momentum=0.9),
                           metrics=['accuracy'])

    @staticmethod
    def get_name():
        return "Triple-scale"

    def fit_from_path(self, pre="None", plot_losses=False, train=True):
        """
        Trains the network, and validates it, with datasets from their directories.
        Increases the training set by adding mirrored and flipped images and using random crops.
        :param pre: Which pre-processed training and validation sets to use.
        :param plot_losses: Boolean indicating whether or not to plot the training history.
        :param train: Boolean indicating whether or not to retrain the model.
        """
        if not train:
            self.model = tf.keras.models.load_model('best_model_yadav.h5')
            return True
        training_75 = image_dataset_from_directory("Data/training/75x75/" + pre,
                                                   image_size=(150, 150),
                                                   batch_size=32,
                                                   class_names=["no_RCS", "RCS"],
                                                   color_mode='grayscale',
                                                   shuffle=True,
                                                   label_mode='categorical')
        training_150 = image_dataset_from_directory("Data/training/150x150/" + pre,
                                                    image_size=(150, 150),
                                                    batch_size=32,
                                                    class_names=["no_RCS", "RCS"],
                                                    color_mode='grayscale',
                                                    shuffle=True,
                                                    label_mode='categorical')
        training_ds = training_75.concatenate(training_150)

        # Increasing the training set by adding mirrored and flipped images
        training_ds = training_ds.concatenate(training_ds.map(dg.mirror_map))
        training_ds = training_ds.concatenate(training_ds.map(dg.flip_map))

        # Increasing the training set by adding images with adjusted brightness
        brightness = training_ds.concatenate(training_ds.map(dg.bright_map_1))
        brightness = brightness.concatenate(training_ds.map(dg.bright_map_2))

        training_ds = brightness.map(dg.standardize_map)

        val_75 = image_dataset_from_directory("Data/validation/75x75/" + pre,
                                              image_size=(150, 150),
                                              batch_size=32,
                                              class_names=["no_RCS", "RCS"],
                                              color_mode='grayscale',
                                              shuffle=True,
                                              label_mode='categorical')
        val_150 = image_dataset_from_directory("Data/validation/150x150/" + pre,
                                               image_size=(150, 150),
                                               batch_size=32,
                                               class_names=["no_RCS", "RCS"],
                                               color_mode='grayscale',
                                               shuffle=True,
                                               label_mode='categorical')
        val_ds = val_75.concatenate(val_150)
        val_ds = val_ds.map(dg.standardize_map)
        callback = tf.keras.callbacks.ModelCheckpoint('best_model_yadav.h5', monitor='val_loss', save_best_only=True)
        history = self.model.fit(training_ds,
                                 epochs=self.epochs,
                                 validation_data=val_ds,
                                 validation_steps=15,
                                 steps_per_epoch=150,
                                 callbacks=[callback])
        self.model = tf.keras.models.load_model('best_model_yadav.h5')

        if plot_losses:
            neural_network_training_plot(history)

    def prediction_score(self, test_data):
        """
        Returns the prediction score for a list of images.
        :param test_data: An array of images to calculate the prediction scores off
        :return: The prediction scores
        """
        scores = []
        for x in tqdm(test_data):
            scales = generate_scales_yadav(x)
            scales = np.array([dg.standardize_map(x.reshape(150, 150, 1), 0)[0] for x in scales])
            results = self.model.predict(scales, verbose=0)
            preds = [np.argmax(x, axis=-1) for x in results]
            z = []
            for i, j in zip(preds[::2], preds[1::2]):
                z.append(max(i, j))
            scores.append(sum(z) / len(z))
        return scores

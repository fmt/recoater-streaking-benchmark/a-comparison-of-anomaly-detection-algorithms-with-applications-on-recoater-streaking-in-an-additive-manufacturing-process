import tensorflow as tf
from tensorflow import keras
from keras.utils import image_dataset_from_directory
import numpy as np
import data_augementation as dg
from visualization import neural_network_training_plot
from tqdm import tqdm
import cv2


class AlexNet:
    model = None

    def __init__(self, epochs):
        """
        Initialization of the AlexNet.
        """
        self.epochs = epochs
        self.model = keras.models.Sequential([
            keras.layers.Conv2D(filters=96, kernel_size=(11, 11), strides=(4, 4), activation='relu',
                                input_shape=(227, 227, 3)),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2)),
            keras.layers.Conv2D(filters=256, kernel_size=(5, 5), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2)),
            keras.layers.Conv2D(filters=384, kernel_size=(3, 3), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.Conv2D(filters=384, kernel_size=(3, 3), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), activation='relu', padding="same"),
            keras.layers.BatchNormalization(),
            keras.layers.MaxPool2D(pool_size=(3, 3), strides=(2, 2)),
            keras.layers.Flatten(),
            keras.layers.Dense(4096, activation='relu'),
            keras.layers.Dropout(0.5),
            keras.layers.Dense(4096, activation='relu'),
            keras.layers.Dropout(0.5),
            keras.layers.Dense(2, activation='softmax')
        ])
        self.model.compile(loss='categorical_crossentropy',
                           optimizer=tf.optimizers.SGD(learning_rate=0.001, momentum=0.9),
                           metrics=['accuracy'])

    @staticmethod
    def get_name():
        return "AlexNet"

    def fit_from_path(self, training_path, validation_path, plot_losses=False, train=True):
        """
        Trains the network, and validates it, with datasets from their directories.
        Increases the amount of training data first by adding mirrored and flipped images and using random crops.
        :param training_path: The path to the training dataset.
        :param validation_path: The path to the validation dataset.
        :param plot_losses: Boolean indicating whether or not to plot the training history.
        :param train: Boolean indicating whether or not to retrain the model.
        """
        if not train:
            self.model = tf.keras.models.load_model('best_model_shi.h5')
            return True
        training_ds = image_dataset_from_directory(training_path,
                                                   batch_size=32,
                                                   class_names=["no_RCS", "RCS"],
                                                   shuffle=True,
                                                   label_mode='categorical')

        # Increasing the training set by adding mirrored and flipped images
        training_ds = training_ds.concatenate(training_ds.map(dg.mirror_map))
        training_ds = training_ds.concatenate(training_ds.map(dg.flip_map))

        # Increasing the training set by adding images with adjusted brightness
        brightness = training_ds.concatenate(training_ds.map(dg.bright_map_1))
        brightness = brightness.concatenate(training_ds.map(dg.bright_map_2))

        # Increasing the training set by creating crops of 227x227 with random offsets
        train_ds = brightness.map(dg.crop_map)
        for _ in range(20):
            train_ds = train_ds.concatenate(brightness.map(dg.crop_map))
        train_ds = train_ds.map(dg.standardize_map)

        train_ds = train_ds.shuffle(buffer_size=200)

        val_ds = image_dataset_from_directory(validation_path,
                                              image_size=(227, 227),
                                              batch_size=32,
                                              class_names=["no_RCS", "RCS"],
                                              shuffle=True,
                                              label_mode='categorical')
        val_ds = val_ds.map(dg.standardize_map)
        callback = tf.keras.callbacks.ModelCheckpoint('best_model_shi.h5', monitor='val_loss', save_best_only=True)
        history = self.model.fit(train_ds,
                                 epochs=self.epochs,
                                 validation_data=val_ds,
                                 validation_steps=10,
                                 steps_per_epoch=100,
                                 callbacks=[callback])
        self.model = tf.keras.models.load_model('best_model_shi.h5')

        if plot_losses:
            neural_network_training_plot(history)

    def predict_from_path(self, test_path):
        """
        Predicts the labels for a dataset from a directory.
        :param test_path: The path to the testing dataset.
        :return: A list of predictions and the list of corresponding labels.
        """
        test_ds = image_dataset_from_directory(test_path,
                                               image_size=(227, 227),
                                               class_names=["no_RCS", "RCS"],
                                               label_mode='categorical')
        test_ds = test_ds.map(dg.standardize_map)
        predictions = np.array([])
        labels = np.array([])
        for x, y in test_ds:
            predictions = np.concatenate([predictions, np.argmax(self.model.predict(x, verbose=0), axis=-1)])
            labels = np.concatenate([labels, np.argmax(y.numpy(), axis=-1)])
        return predictions, labels

    def prediction_score_from_path(self, test_path):
        """
        Returns the prediction scores for a dataset from a directory.
        :param test_path: The path to the testing dataset.
        :return: A list of prediction scores and the list of corresponding labels.
        """
        test_ds = image_dataset_from_directory(test_path,
                                               image_size=(227, 227),
                                               class_names=["no_RCS", "RCS"],
                                               label_mode='categorical')
        test_ds = test_ds.map(dg.standardize_map)
        scores = np.array([])
        labels = np.array([])
        for x, y in tqdm(test_ds):
            preds = self.model.predict(x, verbose=0)
            scores = np.concatenate([scores, [z[1] for z in preds]])
            labels = np.concatenate([labels, np.argmax(y.numpy(), axis=-1)])
        return scores, labels

    def predict(self, images):
        """
        Returns the predictions for a list of images.
        :param images: An array of images to predict.
        :return: The predictions.
        """
        images = [cv2.resize(x, (227, 227)) for x in images]
        images = [tf.image.per_image_standardization(np.expand_dims(np.dstack((x, x, x)), axis=0)) for x in images]
        return [self.model.predict(x, verbose=0) for x in tqdm(images)]

    def evaluate(self, test_path):
        """
        Quick evaluation on a dataset from a directory.
        :param test_path: Path to the directory.
        """
        test_ds = image_dataset_from_directory(test_path,
                                               image_size=(227, 227),
                                               class_names=["no_RCS", "RCS"],
                                               label_mode='categorical')
        test_ds = test_ds.map(dg.standardize_map)
        self.model.evaluate(test_ds)

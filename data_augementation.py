import tensorflow as tf


def mirror_map(image, label):
    """
    Mirrors the images in a batch.
    :param image: The image.
    :param label: Corresponding label.
    :return: The mirrored image with the label.
    """
    return tf.image.flip_left_right(image), label


def flip_map(image, label):
    """
    Flips the images in a batch.
    :param image: The image.
    :param label: Corresponding label.
    :return: The flipped image with the label.
    """
    return tf.image.flip_up_down(image), label


def bright_map_1(image, label):
    """
    Changes the brightness of the images in a batch.
    :param image: The image.
    :param label: Corresponding label.
    :return: The image with adjusted brightness with the label.
    """
    return tf.image.adjust_brightness(image, 0.4), label


def bright_map_2(image, label):
    """
    Changes the brightness of the images in a batch.
    :param image: The image.
    :param label: Corresponding label.
    :return: The image with adjusted brightness with the label.
    """
    return tf.image.adjust_brightness(image, -0.4), label


def crop_map(image, label):
    """
    Crops the images in a batch.
    Crops to the size of 227x227 but with a random offset.
    :param image: The image.
    :param label: Corresponding label.
    :return: The cropped image with the label.
    """
    return tf.image.random_crop(value=image, size=[tf.shape(image)[0], 227, 227, 3]), label


def standardize_map(image, label):
    """
    Standardizes the images in a batch to ensure a mean of 0 and a std of 1 for all pixels.
    :param image: The image.
    :param label: Corresponding label.
    :return: The standardized image with the label.
    """
    return tf.image.per_image_standardization(image), label

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sklearn.metrics import roc_curve
from tqdm import tqdm


class LineProfiles:

    method = None
    threshold = None
    window_size = None

    def __init__(self, method="moving", window_size=4):
        self.method = method
        self.window_size = window_size

    @staticmethod
    def get_name():
        return "Line profiles"

    @staticmethod
    def func(x, a, b, c):
        return a * x ** 2 + b * x + c

    @staticmethod
    def line_profiles(image):
        """
        Calculates the line profile of a gray scaled image.
        :param image: Gray scaled image
        :return: Line profile
        """
        return [np.mean(x) for x in image]

    def fit(self, data, labels):
        """
        Calculates the optimal threshold based on the input data.
        :param data: The training data
        :param labels: The labels.
        :return: The optimal threshold
        """
        scores = [self.prediction_score(x) for x in data]
        fpr, tpr, thresholds = roc_curve(labels, scores)
        self.threshold = thresholds[np.argmax(tpr - fpr)]
        return self.threshold

    def predict(self, images):
        """
        Predicts whether recoater streaking occurs in a list of images.
        :param images: List of images
        :return: List of predictions.
        """
        return [self.prediction_score(x) > self.threshold for x in tqdm(images)]

    def prediction_score(self, img):
        """
        Returns the prediction score for an image.
        :param img: The image to calculate the prediction score for
        :return: The prediction score
        """
        line_profile = self.line_profiles(img)
        if self.method == "moving":
            averages = self.simple_moving_average(line_profile)
            return max(np.absolute(np.array(averages) - np.array(line_profile[:len(averages)])))
        else:
            xdata = [*range(len(line_profile))]
            (a, b, c), _ = curve_fit(self.func, xdata, line_profile)
            fitted = [a * x ** 2 + b * x + c for x in xdata]
            return max(np.absolute(np.array(fitted) - np.array(line_profile)))

    def simple_moving_average(self, line_profile):
        """
        Calculates the moving average.
        :param line_profile: Line profile as array
        :return: The moving average
        """
        n = self.window_size
        ret = np.cumsum(line_profile, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n

    def indicate_streaking(self, img):
        """
        Searches for recoater streaming in an image and returns the index where it occurs.
        :param img: The image
        :return: The indices of recoater streaking
        """
        line_profile = self.line_profiles(img)
        if self.method == "moving":
            averages = self.simple_moving_average(line_profile)
            return np.where(np.absolute(np.array(averages) - np.array(line_profile[:len(averages)])) > self.threshold)
        else:
            xdata = [*range(len(line_profile))]
            (a, b, c), _ = curve_fit(self.func, xdata, line_profile)
            fitted = [a * x ** 2 + b * x + c for x in xdata]
            return np.where(np.absolute(np.array(fitted) - np.array(line_profile)) > self.threshold)

    def plot_line_profile(self, image, name):
        """
        Plots the line profile together with prediction curves.
        :param image: The image to plot all information
        :param name: The name of the print + layer number
        """
        line_profile = self.line_profiles(image)
        moving_average = self.simple_moving_average(line_profile)
        xdata = [*range(len(line_profile))]
        (a, b, c), _ = curve_fit(self.func, xdata, line_profile)
        fitted = [a * x ** 2 + b * x + c for x in xdata]
        plt.plot(line_profile, label="profile")
        plt.plot(fitted, label="fitted")
        plt.plot(moving_average, label="SMA")
        plt.grid()
        plt.legend(loc="upper left")
        plt.title("Line profile")
        plt.suptitle(name)
        plt.show()

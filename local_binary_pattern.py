from skimage.feature import local_binary_pattern
from skimage.measure import block_reduce
from sklearn import svm
import numpy as np
from tqdm import tqdm


class LocalBinaryPattern:
    radius = 0
    n_points = 8 * radius
    height = None
    width = None
    part_size = None
    clf = None
    method = None

    def __init__(self, radius=1, block=None, partition_size=None, method="uniform"):
        self.radius = radius
        self.n_points = 8 * radius
        self.block = block if block > 1 else None
        self.part_size = partition_size if partition_size > 1 else None
        self.method = method

    @staticmethod
    def get_name():
        return "LBP"

    def fit(self, images, labels):
        """
        Trains the model by calculating the local binary patterns, generating the histograms and fitting it into an SVM.
        :param images: The list of training images
        :param labels: The list of corresponding labels
        """
        lbps = [self.get_local_binary_patterns(x) for x in tqdm(images)]
        histograms = [self.get_histograms(x) for x in tqdm(lbps)]
        self.clf = svm.LinearSVC(C=100, dual=False)
        self.clf.fit(histograms, labels)

    def get_local_binary_patterns(self, image):
        """
        Calculates the local binary patterns. If selected, will first partition the images accordingly.
        :param image: The image to calculate local binary patterns of.
        :return: The local binary patterns
        """
        if self.block is not None:
            image = block_reduce(image, block_size=self.block, func=np.mean)
        return local_binary_pattern(image, self.n_points, self.radius, method=self.method)

    def get_histograms(self, lbps):
        """
        Calculates the histogram of the local binary patterns of an image.
        :param lbps: Local binary patterns of an image
        :return: The histogram of the local binary patterns
        """
        if self.part_size is None:
            histogram = np.histogram(lbps, bins=np.arange(0, self.n_points + 3), range=(0, self.n_points + 2))[0]
            histogram = histogram.astype("float")
            histogram /= histogram.sum()
            return histogram
        histograms = []
        rows, cols = lbps.shape
        for i in range(0, rows // self.part_size):
            for j in range(0, cols // self.part_size):
                histogram = np.histogram(lbps[i*self.part_size:(i+1)*self.part_size,
                                         j*self.part_size:(j+1)*self.part_size],
                                         bins=np.arange(0, self.n_points + 3),
                                         range=(0, self.n_points + 2))[0]
                histogram = histogram.astype("float")
                histogram /= histogram.sum()
                histograms.append(histogram)
        return [histogram for sublist in histograms for histogram in sublist]

    def predict(self, images):
        """
        Detect recoater streaking on a list of images.
        :param images: The images to detect recoater streaking on.
        :return: A list of predicted labels.
        """
        lbps = [self.get_local_binary_patterns(x) for x in tqdm(images)]
        histograms = [self.get_histograms(x) for x in tqdm(lbps)]
        return self.clf.predict(histograms)

    def scores(self, images):
        """
        Predict confidence scores for classification images
        :param images: The images to detect recoater streaking on.
        :return: A list of Confidence scores.
        """
        lbps = [self.get_local_binary_patterns(x) for x in tqdm(images)]
        histograms = [self.get_histograms(x) for x in tqdm(lbps)]
        return self.clf.decision_function(histograms)

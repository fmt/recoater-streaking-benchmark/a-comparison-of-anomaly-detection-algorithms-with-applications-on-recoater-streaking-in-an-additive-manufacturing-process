import keras
from keras.applications.efficientnet import EfficientNetB7
from keras.utils import image_dataset_from_directory
from keras.models import Sequential
from keras import layers
import tensorflow as tf
import data_augementation as dg
import numpy as np
from tqdm import tqdm
import cv2


class EfficientNet:

    def __init__(self, epochs, pretrained=True):
        self.epochs = epochs
        self.RESOLUTION = 600

        # EfficientNetB0 	224
        # EfficientNetB1 	240
        # EfficientNetB2 	260
        # EfficientNetB3 	300
        # EfficientNetB4 	380
        # EfficientNetB5 	456
        # EfficientNetB6 	528
        # EfficientNetB7 	600

        img_augmentation = Sequential(
            [
                layers.RandomRotation(factor=0.15),
                layers.RandomTranslation(height_factor=0.1, width_factor=0.1),
                layers.RandomFlip(),
                layers.RandomContrast(factor=0.2),
            ],
            name="img_augmentation",
        )

        inputs = layers.Input(shape=(self.RESOLUTION, self.RESOLUTION, 3))
        x = img_augmentation(inputs)
        if pretrained:
            model = EfficientNetB7(include_top=False, weights='imagenet', input_tensor=x)
            model.trainable = False
            x = layers.GlobalAveragePooling2D()(model.output)
            x = layers.BatchNormalization()(x)
            x = layers.Dropout(rate=0.4)(x)
            outputs = layers.Dense(2, activation='softmax')(x)
        else:
            outputs = EfficientNetB7(include_top=True,
                                     weights=None,
                                     classes=2)(x)

        self.model = keras.Model(inputs, outputs)
        self.model.compile(loss='categorical_crossentropy',
                           optimizer=keras.optimizers.Adam(learning_rate=1e-3),
                           metrics=['accuracy'])

    @staticmethod
    def get_name():
        return "EfficientNetB7"

    def fit_from_path(self, training_path, validation_path, train=True):
        """
        Trains the network, and validates it, with datasets from their directories.
        :param training_path: The path to the training dataset.
        :param validation_path: The path to the validation dataset.
        :param train: Boolean indicating whether or not to retrain the model.
        """
        if not train:
            self.model = tf.keras.models.load_model('best_model_efficient.h5')
            return True
        training_ds = image_dataset_from_directory(training_path,
                                                   image_size=(self.RESOLUTION, self.RESOLUTION),
                                                   batch_size=8,
                                                   class_names=["no_RCS", "RCS"],
                                                   shuffle=True,
                                                   label_mode='categorical')
        training_ds = training_ds.map(dg.standardize_map)
        training_ds = training_ds.repeat(self.epochs)

        val_ds = image_dataset_from_directory(validation_path,
                                              image_size=(self.RESOLUTION, self.RESOLUTION),
                                              batch_size=8,
                                              class_names=["no_RCS", "RCS"],
                                              shuffle=True,
                                              label_mode='categorical')
        val_ds = val_ds.map(dg.standardize_map)
        val_ds = val_ds.repeat(self.epochs)
        callback = tf.keras.callbacks.ModelCheckpoint('best_model_efficient.h5', monitor='val_loss',
                                                      save_best_only=True)
        self.model.fit(training_ds,
                       epochs=self.epochs,
                       validation_data=val_ds,
                       validation_steps=10,
                       steps_per_epoch=100,
                       callbacks=[callback])
        self.model = tf.keras.models.load_model('best_model_efficient.h5')

    def prediction_score_from_path(self, test_path):
        """
        Returns the prediction scores for a dataset from a directory.
        :param test_path: The path to the testing dataset.
        :return: A list of prediction scores and the list of corresponding labels.
        """
        test_ds = image_dataset_from_directory(test_path,
                                               image_size=(self.RESOLUTION, self.RESOLUTION),
                                               class_names=["no_RCS", "RCS"],
                                               label_mode='categorical')
        test_ds = test_ds.map(dg.standardize_map)
        scores = np.array([])
        labels = np.array([])
        for x, y in tqdm(test_ds):
            scores = np.concatenate([scores, [z[1] for z in self.model.predict(x, verbose=0)]])
            labels = np.concatenate([labels, np.argmax(y.numpy(), axis=-1)])
        return scores, labels

    def predict(self, test_data):
        """
        Returns the prediction score for a list of images.
        :param test_data: An array of images to calculate the prediction scores off
        :return: The prediction scores
        """
        images = [cv2.resize(x, (self.RESOLUTION, self.RESOLUTION)) for x in test_data]
        images = [tf.image.per_image_standardization(np.expand_dims(np.dstack((x, x, x)), axis=0)) for x in images]
        return [self.model.predict(x, verbose=0) for x in tqdm(images)]
